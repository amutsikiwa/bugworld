import java.io.IOException;
public class Main {
    public static World world = new World();
    public static void main(String[] args) throws IOException {
        try{
            //read the input file
            world.loadWorldMapData();
            //world.displayWorldData();
            world.loadWorldMap();
            world.displayWorldCellData();
            System.out.println("The World X dimension: " + world.getSizeX());
            System.out.println("The World Y dimension: " + world.getSizeY());
        }catch(Exception err){
            err.printStackTrace();;
        }

    }

}
