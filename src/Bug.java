/**
 * @author Mutsikiwa Admire
 * created 21 April 2018
 * purpose is to implement the Bug C;ass
 *User: World Clas
 */
public class Bug {
    private int bugID;
    private BugColor color;  // the enumerated data type BugColor
    private int state ;
    private int direction ;
    private boolean hasFood;
    private boolean isDead ;
    private boolean resting ;

    public Bug(int bugID, BugColor color, int state, int direction, boolean hasFood, boolean isDead, boolean resting) {
        this.bugID = bugID;
        this.color = color;
        this.state = state;
        this.direction = direction;
        this.hasFood = hasFood;
        this.isDead = isDead;
        this.resting = resting;
    }

    public int getBugID() {
        return bugID;
    }

    public void setBugID(int bugID) {
        this.bugID = bugID;
    }

    public BugColor getColor() {
        return color;
    }

    public void setColor(BugColor color) {
        this.color = color;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public boolean isHasFood() {
        return hasFood;
    }

    public void setHasFood(boolean hasFood) {
        this.hasFood = hasFood;
    }

    public boolean isDead() {
        return isDead;
    }

    public void setDead(boolean dead) {
        isDead = dead;
    }

    public boolean isResting() {
        return resting;
    }

    public void setResting(boolean resting) {
        this.resting = resting;
    }
    private void pickup(){};
    private void drop(){};
    private void turn(){};
    private void move() {};
    private void flip(){};
    private void turn(String directionOfTurning){};
    private boolean dead(Bug bug) {return false;};
    private int[][] position(Bug bug){int[][] pos = new int[0][0]; return pos;};
    private void kill(int xCoordicate,int yCoordinates) {};
    private int food_at(int xCoordicate,int yCoordinates) {return 0;};
}
