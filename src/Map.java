
import javax.swing.*;
import java.awt.*;
import java.util.Random;
public class Map extends JPanel {
    public World world = new World(); // to be able to access the Cell[][] cells object so that the map can render object as a function of Cell attributes

    public static final Color CLEAR_CELL = new Color(255,255,255);
   //ublic static final Color CLEAR_CELL = new Color(255,204,102);
    public static final Color WITH_OBSTACLE_CELL = new Color(153,102,0);
    public static final Color BLACK_BUG_HOME_CELL = new Color(96,96,96);
    public static final Color WITH_FOOD_CELL = new Color(51,153,0);
   //ublic static final Color CLEAR_CELL = new Color(0,153,153);
   //ublic static final Color CLEAR_CELL = new Color(102,102,255);
   //ublic static final Color CLEAR_CELL = new Color(0,0,153);
   //ublic static final Color CLEAR_CELL = new Color(51,51,0); Black
    public static final Color RED_BUG_HOME_CELL = new Color(155, 92, 119);

    public static final Color[] TERRAIN = {
            CLEAR_CELL,
            WITH_OBSTACLE_CELL,
            BLACK_BUG_HOME_CELL,
            WITH_FOOD_CELL,
            RED_BUG_HOME_CELL
    };

    public static final int NUM_ROWS = 10;//  take value of the sizeY
    public static final int NUM_COLS =10 ;//take value of the sizeX

    public static final int PREFERRED_GRID_SIZE_PIXELS = 10;

    // In reality you will probably want a class here to represent a map tile,
    // which will include things like dimensions, color, properties in the
    // game world.  Keeping simple just to illustrate.
    private final Color[][] terrainGrid;

    public Map(){
        this.terrainGrid = new Color[NUM_ROWS][NUM_COLS];
        Random r = new Random();
        // Randomize the terrain
        Color randomColor = TERRAIN[0];
        for (int i = 0; i < NUM_ROWS; i++) {
            for (int j = 0; j < NUM_COLS; j++) {
                // this is where cell object such as foodParticle, objetcs will be displayed.
                    int randomTerrainIndex = r.nextInt(TERRAIN.length);
                    randomColor = TERRAIN[randomTerrainIndex];
                      this.terrainGrid[i][j] = randomColor;
               // System.out.println("[" + i + "]" + "[" + i +"]");
            }
        }
        int preferredWidth = NUM_COLS * PREFERRED_GRID_SIZE_PIXELS;
        int preferredHeight = NUM_ROWS * PREFERRED_GRID_SIZE_PIXELS;
        setPreferredSize(new Dimension(preferredWidth, preferredHeight));
    }

    @Override
    public void paintComponent(Graphics g) {
        // Important to call super class method
        super.paintComponent(g);
        // Clear the board
        g.clearRect(0, 0, getWidth(), getHeight());
        // Draw the grid
        int rectWidth = getWidth() / NUM_COLS;
        int rectHeight = getHeight() / NUM_ROWS;

        for (int i = 0; i < NUM_ROWS; i++) {
            for (int j = 0; j < NUM_COLS; j++) {
                // Upper left corner of this terrain rect
                int x = i * rectWidth;
                int y = j * rectHeight;
                Color terrainColor = terrainGrid[i][j];
                g.setColor(terrainColor);
                g.fillRect(x, y, rectWidth, rectHeight);
                //it is here where cell object has to be rendered  as a function of the x,y  cell addressing
                System.out.println("[" + i + "]" + "[" + i +"]");
            }
        }
    }

    public static void main(String[] args) {
        // http://docs.oracle.com/javase/tutorial/uiswing/concurrency/initial.html
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new JFrame("Game");
                Map map = new Map();
                frame.add(map);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.pack();
                frame.setVisible(true);
            }
        });
    }
}

// Based on https://stackoverflow.com/questions/8611268/java-creating-a-2d-tile-map-in-a-panel-using-graphics2d-rectangles
//there is need to map the 2 dimensional array cell object to this.  There is need to create a cell color object to reflect this colors if desired
// need to remove the random number and base the Cell color on the cell color field. i.e cells[i][j].color