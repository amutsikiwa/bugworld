/**
 *
 * @author  Mutsikiwa Admire and Leslie Nyandoto
 * created 21 March 2018
 * purpose to Implement enumerated color data type for the bug color member data
 */
public enum BugColor {
    BLACK,
    RED
}
