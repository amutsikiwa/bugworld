/**
 * @author : Mutsikiwa Admire and Leslie Nyandoro
 * created 21 April 2018
 * Purpose: models the Cell class
 * User: World Class
 *
 */
public class Cell {
    //private int positionX; // not sure about the use of this field ???
    //private int positionY; // not sure about  the use of this field ???
    private int coordinateX;
    private int coordinateY;
    public Bug bug;
    // to be moved to constant class
    public final String BLACKBUGHOMESYMBOL="-";
    public final String  REDBUGHOMESYMBOL="+";
    public static final int MARKER_ON = 1; // To be used be used to turn marker on
    public static final int MARKER_OFF = 0 ;
    private boolean hasObstacle; // there is need to document this change
    private int numberOfFoodParticles ;  // there is need to document this change
    private boolean isHomeForReds ;
    private boolean isHomeForBlacks;
    // private  boolean homeArea ; // to be changed as we can not  implement l which homearea a cell belongs to. either red or black bugg
    private int[] blackMarkers = new int[5];  // introduced to be able to implement functions expressed under 2.5 Chemistry of the specifications
    private int[] redMarkers = new int[5] ;
    //private Bug bug; // Consider putting this in an interface class to observe the open close principle
   //accessor methods
    //we cannot have a cell without coordinates hence the constructor has to have at least the X,Y coordinates
    //Constructors
    public Cell(int coordinateX, int coordinateY,boolean hasObstacle, int numberOfFoodParticles) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.hasObstacle = hasObstacle ;
        this.isHomeForBlacks = false;
        this.isHomeForReds = false;
        this.numberOfFoodParticles = numberOfFoodParticles;
        bug = null;
    }
    public Cell(int coordinateX, int coordinateY, int numberOfFoodParticles) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.hasObstacle = false ;
        this.numberOfFoodParticles = numberOfFoodParticles;
        this.isHomeForReds=false;
        this.isHomeForBlacks=true;
        bug = null;

    }

    public Cell(int coordinateX, int coordinateY,boolean hasObstacle) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.hasObstacle = hasObstacle ;
        this.numberOfFoodParticles = 0;
        this.isHomeForReds=false;
        this.isHomeForBlacks=true;
        bug = null;

    }
    public Cell(int coordinateX, int coordinateY) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.hasObstacle = false ;
        this.numberOfFoodParticles = 0;
        this.isHomeForReds=false;
        this.isHomeForBlacks=false;
        bug = null;

    }

    public Cell(int coordinateX, int coordinateY, String readSymbol) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.hasObstacle = false ;
        this.numberOfFoodParticles = 0;
        // this is where a bug object should be created
        bug = null;
        // to change this
        if (readSymbol.equals(BLACKBUGHOMESYMBOL) && !readSymbol.equals(".")){
            this.isHomeForBlacks= true;
        }else if(readSymbol.equals(REDBUGHOMESYMBOL) && !readSymbol.equals(".")) {
            this.isHomeForReds = true;
        }
    }

    public int getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(int coordinateX) {
        this.coordinateX = coordinateX;
    }

    public int getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(int coordinateY) {
        this.coordinateY = coordinateY;
    }

    //getter and setter for Cell data. These require cell object as an augument


    public boolean isHasObstacle() {
        return hasObstacle;
    }

    public void setHasObstacle(boolean hasObstacle) {
        this.hasObstacle = hasObstacle;
    }

    public int getNumberOfFoodParticles() {
        return numberOfFoodParticles;
    }

    public void setNumberOfFoodParticles(int numberOfFoodParticles) {
        this.numberOfFoodParticles = numberOfFoodParticles;
    }

    public boolean isHomeForReds() {
        return isHomeForReds;
    }

    public void setHomeForReds(boolean homeForReds) {
        isHomeForReds = homeForReds;
    }

    public boolean isHomeForBlacks() {
        return isHomeForBlacks;
    }

    public void setHomeForBlacks(boolean homeForBlacks) {
        isHomeForBlacks = homeForBlacks;
    }

    //still to implement marker functions
    private void marker(int coordinateX,int coordinateY, BugColor bugColor,int markBit) {
        // Given the cell coordinates, bug color and the markbit to mark it turns the corresponding marker
        if (bugColor.equals(BugColor.BLACK)){ // marker for black bug => use the relevant marker array
            this.blackMarkers[markBit]= MARKER_ON; // set the bit on
        }else if(bugColor.equals(BugColor.RED)) { // marker for red bug => use the relevant marker array
            this.redMarkers[markBit] = MARKER_ON;
        }
    }
    private void unmarker(int coordinateX,int coordinateY, BugColor bugColor,int markBit) {
        // Given the cell coordinates, bug color and the markbit to mark it turns the corresponding marker
        if (bugColor.equals(BugColor.BLACK)){ // marker for black bug => use the relevant marker array
            this.blackMarkers[markBit]= MARKER_OFF; // set the bit on
        }else if(bugColor.equals(BugColor.RED)) { // marker for red bug => use the relevant marker array
            this.redMarkers[markBit] = MARKER_OFF;
        }
    }
}

